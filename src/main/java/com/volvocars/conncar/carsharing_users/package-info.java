/* Copyright (C) 2018 Volvo. All Rights Reserved. */

/** Implementation of the carsharing users core module. */
package com.volvocars.conncar.carsharing_users;
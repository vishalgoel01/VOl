/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.conncar.carsharing_users.Models;

import java.util.UUID;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.datastax.driver.core.DataType;

/** POJO representing a Person resource. */
@Table("Person")
public class PersonEntity {

	/** Id of the Person. */
	@PrimaryKey
	@CassandraType(type = DataType.Name.UUID)
	private UUID id = null;

	/** First Name of the Person. */
	private String firstName = null;

	/** Last Name of the Person. */
	private String lastName = null;

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "PersonEntity [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}

/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.conncar.carsharing_users.Models;

import java.util.UUID;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.datastax.driver.core.DataType;

@Table("Vehicle")
public class VehicleEntity {

	/** Id of the Vehicle. */
	@PrimaryKey
	@CassandraType(type = DataType.Name.UUID)
	private UUID id = null;

	/** VIN of the Vehicle. */
	private String vin = null;

	/** User of the Vehicle. */
	private UserEntity owner = null;

	/** Model of the Vehicle. */
	private String model = null;

	/** Color of the Vehicle. */
	private String colour = null;

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the vin
	 */
	public String getVin() {
		return vin;
	}

	/**
	 * @param vin
	 *            the vin to set
	 */
	public void setVin(String vin) {
		this.vin = vin;
	}

	/**
	 * @return the owner
	 */
	public UserEntity getOwner() {
		return owner;
	}

	/**
	 * @param owner
	 *            the owner to set
	 */
	public void setOwner(UserEntity owner) {
		this.owner = owner;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the colour
	 */
	public String getColour() {
		return colour;
	}

	/**
	 * @param colour
	 *            the colour to set
	 */
	public void setColour(String colour) {
		this.colour = colour;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "VehicleEntity [id=" + id + ", vin=" + vin + ", owner=" + owner + ", model=" + model + ", colour="
				+ colour + "]";
	}

}

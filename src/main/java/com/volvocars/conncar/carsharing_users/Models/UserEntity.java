/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.conncar.carsharing_users.Models;

import java.util.UUID;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.datastax.driver.core.DataType;

/** POJO representing a User resource. */
@Table("User")
public class UserEntity {

	/** Id of the User. */
	@PrimaryKey
	@CassandraType(type = DataType.Name.UUID)
	private UUID id = null;

	/** Id of the User. */
	private String userId = null;

	/** Person entity of the User. */
	private PersonEntity person = null;

	/** LegalEEntity of the User. */
	private LegalEEntity company = null;

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the person
	 */
	public PersonEntity getPerson() {
		return person;
	}

	/**
	 * @param person
	 *            the person to set
	 */
	public void setPerson(PersonEntity person) {
		this.person = person;
	}

	/**
	 * @return the company
	 */
	public LegalEEntity getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(LegalEEntity company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", userId=" + userId + ", person=" + person + ", company=" + company + "]";
	}

}

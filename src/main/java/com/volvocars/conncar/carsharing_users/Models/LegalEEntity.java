/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.conncar.carsharing_users.Models;

import java.util.UUID;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.datastax.driver.core.DataType;

/** POJO representing a Legal resource. */
@Table("Legal")
public class LegalEEntity {

	/** Id of the Legal. */
	@PrimaryKey
	@CassandraType(type = DataType.Name.UUID)
	private UUID id = null;

	/** Name of the Legal. */
	private String name = null;

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "LegalEEntity [id=" + id + ", name=" + name + "]";
	}

}

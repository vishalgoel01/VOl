/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.conncar.carsharing_users;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.volvocars.conncar.carsharing_users.Models.VehicleEntity;

/** repository for Vehicle resource */
@Repository
public interface VehicleRepository extends CassandraRepository<VehicleEntity> {

}

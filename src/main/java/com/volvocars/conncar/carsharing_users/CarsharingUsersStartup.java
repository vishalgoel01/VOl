/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.conncar.carsharing_users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/** Controller operating on core vehicle calendar application. */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.volvocars.conncar.carsharing_users" })
public class CarsharingUsersStartup {

	/** Main method to start application. */
	public static void main(String[] args) throws Exception {
		SpringApplication.run(CarsharingUsersStartup.class, args);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

}
/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.conncar.carsharing_users;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.java.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

/** Cassandra Configuration file. */
@Configuration
@PropertySource(value = { "classpath:cassandra.properties" })
@EnableCassandraRepositories(basePackages = "com.volvocars.conncar.carsharing_users")
@EnableAutoConfiguration
public class CassandraConfig extends AbstractCassandraConfiguration {

	/** Class logger. */
	private static final Log LOGGER = LogFactory.getLog(CassandraConfig.class);

	/** Environment of application. */
	@Autowired
	private Environment environment;

	/** Username of database. */
	@Value("${cassandra.username}")
	private String CASSANDRA_USERNAME;

	/** Password of database. */
	@Value("${cassandra.password}")
	private String CASSANDRA_PASSWORD;

	/** keyspace of database. */
	@Value("${cassandra.keyspace}")
	private static String CASSANDRA_KEYSPACE;

	@Override
	protected String getKeyspaceName() {
		return environment.getProperty("cassandra.keyspace");
		// return CASSANDRA_KEYSPACE;
	}

	@Override
	@Bean
	public CassandraClusterFactoryBean cluster() {
		final CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
		cluster.setContactPoints(environment.getProperty("cassandra.contactpoints"));
		cluster.setPort(Integer.parseInt(environment.getProperty("cassandra.port")));
		cluster.setUsername(CASSANDRA_USERNAME);
		cluster.setPassword(CASSANDRA_PASSWORD);
		LOGGER.info("Cluster created with contact points [" + environment.getProperty("cassandra.contactpoints") + "] "
				+ "& port [" + Integer.parseInt(environment.getProperty("cassandra.port")) + "].");
		return cluster;
	}

	@Override
	@Bean
	public CassandraMappingContext cassandraMapping() throws ClassNotFoundException {
		return new BasicCassandraMappingContext();
	}

	// @Override
	// protected List<CreateKeyspaceSpecification> getKeyspaceCreations() {
	// return
	// Collections.singletonList(CreateKeyspaceSpecification.createKeyspace(CASSANDRA_KEYSPACE).ifNotExists()
	// .with(KeyspaceOption.DURABLE_WRITES, true).withSimpleReplication());
	// }

	@Override
	public String[] getEntityBasePackages() {
		return new String[] { "com.volvocars.conncar.core.vehicle_calendar" };
	}

	// @Override
	// protected List<String> getStartupScripts() {
	// String KEYSPACE_QUERY = "CREATE KEYSPACE IF NOT EXISTS volvo WITH
	// replication = { 'class': 'SimpleStrategy', 'replication_factor':
	// '3'};";
	// String TABLE_QUERY = "CREATE TABLE IF NOT EXISTS volvo.Entry ( id UUID,
	// vin text, startTime timestamp, endTime timestamp, function text,
	// exclusive boolean, data text, PRIMARY KEY ( vin, id
	// ,function,startTime,endTime));";
	// String VIEW_START_TIME = "CREATE INDEX IF NOT EXISTS entrystarttime ON
	// volvo.entry (startTime);";
	// String VIEW_END_TIME = "CREATE INDEX IF NOT EXISTS entryendTime ON
	// volvo.entry (endTime);";
	// String VIEW3_FUNCTION = "CREATE INDEX IF NOT EXISTS entryfunction ON
	// volvo.entry (function);";
	// return Arrays.asList(TABLE_QUERY, VIEW_START_TIME, VIEW_END_TIME,
	// VIEW3_FUNCTION);
	// }
}
#!/bin/bash
java -Xmx1g -Xms1g -XX:+UseG1GC -XX:ParallelGCThreads=20 -XX:ConcGCThreads=5 -XX:InitiatingHeapOccupancyPercent=80 -XX:MaxGCPauseMillis=200 -Dlogging.config=logback.xml -jar /carsharing-users.jar
